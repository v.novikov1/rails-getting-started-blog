class Article < ApplicationRecord
  include Visible
  after_initialize :init
  def init
    self.status ||= 'public' #will set the default value only if it's nil
  end

  VALID_STATUSES = %w[public private archived].freeze
  validates :status, inclusion: { in: VALID_STATUSES }
  has_many :comments, dependent: :destroy
  validates :title, presence: true
  validates :body, presence: true, length: { minimum: 10 }
end
